RSpec::Matchers.define :match_schema do |version: '2.0'|
  def schema_for(version)
    License::Management.root.join("spec/fixtures/schema/v#{version}.json").to_s
  end
  match do |actual|
    !actual.nil? && JSON::Validator.fully_validate(schema_for(version), actual).empty?
  end

  failure_message do |response|
    "didn't match the schema for version #{version}" \
    " The validation errors were:\n#{@errors.join("\n")}"
  end
end
