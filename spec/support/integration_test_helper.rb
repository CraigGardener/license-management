module IntegrationTestHelper
  class IntegrationTestRunner
    attr_reader :project_path

    def initialize(project_path = File.join(Dir.pwd, 'tmp', SecureRandom.uuid))
      FileUtils.mkdir_p(project_path)
      @project_path = project_path
    end

    def add_file(name, content = nil)
      full_path = Pathname.new("#{project_path}/#{name}")
      FileUtils.mkdir_p(full_path.dirname)
      IO.write(full_path, block_given? ? yield : content)
    end

    def clone(repo)
      execute({}, "git clone #{repo} #{project_path}")
    end

    def scan(env: {})
      return {} unless execute(env, './bin/test-local', project_path)

      report_path = "#{project_path}/gl-license-management-report.json"
      return {} unless File.exist?(report_path)

      JSON.parse(IO.read(report_path), symbolize_names: true)
    end

    def execute(env = {}, *args)
      Bundler.with_clean_env do
        system(env, *args)
      end
    end

    def cleanup
      FileUtils.rm_rf(project_path) if Dir.exist?(project_path)
    end
  end

  def runner(*args)
    @runner ||= IntegrationTestRunner.new(*args)
  end

  def find_in(report, name)
    report[:dependencies].find do |dependency|
      dependency[:name] == name
    end
  end
end
