FROM licensefinder/license_finder:5.11.1
ENV PATH="${PATH}:/root/.asdf/shims:/root/.asdf/bin"
ENV LM_HOME=/opt/license-management
ENV LM_PYTHON_VERSION 3
ENV LM_REPORT_VERSION ${LM_REPORT_VERSION:-2}
RUN cd /tmp && \
    wget --quiet --no-cookies https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.5%2B10/OpenJDK11U-jdk_x64_linux_hotspot_11.0.5_10.tar.gz -O jdk-11.tgz && \
    tar xf /tmp/jdk-11.tgz && \
    mv jdk-11.0.5+10 /usr/lib/jvm/adoptopen_jdk11 && \
    rm /tmp/jdk-11.tgz
RUN npm install npm-install-peers

# Don't let Rubygem fail with the numerous projects using PG or MySQL,
# install realpath, includes for python3, and pip for python3
# Install additional php packages for better composer package support
RUN add-apt-repository ppa:ondrej/php -y && apt-get update -y && \
    apt-get upgrade -y --no-install-recommends && \
    apt-get install -y --no-install-recommends \
    bsdmainutils \
    libjpeg8-dev \
    zlib1g-dev \
    libpq-dev libmysqlclient-dev realpath \
    php7.1-mbstring php7.1-intl php7.1-xml php7.1-soap -y && \
    git clone --depth 1 --branch v0.7.6 https://github.com/asdf-vm/asdf.git $HOME/.asdf && \
    echo 'pip' >> $HOME/.default-python-packages && \
    echo 'setuptools' >> $HOME/.default-python-packages && \
    echo '\n. $HOME/.asdf/asdf.sh' >> $HOME/.bashrc && \
    asdf plugin-add python && \
    echo 'python 3.8.1 3.5.9 2.7.17' >> $HOME/.tool-versions && \
    asdf install && \
    asdf global python 3.8.1 && \
    asdf reshim && \
    rm -rf /var/lib/apt/lists/*

COPY config/NuGet /root/.config/NuGet
COPY config/gradle /root/.gradle
COPY test /test
COPY run.sh /
COPY . /opt/license-management/
RUN bash -lc "cd /opt/license-management && gem build *.gemspec && gem install *.gem"
ENTRYPOINT ["/run.sh"]
