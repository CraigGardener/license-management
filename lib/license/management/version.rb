# frozen_string_literal: true

module License
  module Management
    VERSION = '2.4.3'
  end
end
