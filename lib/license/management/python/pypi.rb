# frozen_string_literal: true

require 'net/hippie'

module License
  module Management
    class PyPI
      include Loggable

      def initialize(http)
        @http = http
      end

      def definition_for(name, version)
        uri = "https://pypi.org/pypi/#{name}/#{version}/json"
        process(@http.with_retry { |client| client.get(uri) }).tap do |definition|
          log_info([name, version, definition["license"]].inspect)
        end
      rescue *Net::Hippie::CONNECTION_ERRORS
        {}
      end

      class << self
        def definition(name, version)
          @pypi ||= new(License::Management.http)
          @pypi.definition_for(name, version)
        end
      end

      private

      def process(response)
        return JSON.parse(response.body).fetch('info', {}) if ok?(response)

        log_error([response.class, response.code, response.body].inspect)
        {}
      end

      def ok?(response)
        response.is_a?(Net::HTTPSuccess)
      end
    end
  end
end
