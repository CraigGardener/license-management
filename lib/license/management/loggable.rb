# frozen_string_literal: true

module License
  module Management
    module Loggable
      def logger
        ::LicenseFinder::Core.default_logger
      end

      def log_info(message)
        logger.info(self.class, message)
      end

      def log_error(message)
        logger.info(self.class, message, color: :red)
      end
    end
  end
end
