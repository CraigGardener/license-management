# frozen_string_literal: true

require 'pathname'
require 'yaml'
require 'json'
require 'license_finder'
require 'license/management/loggable'
require 'license/management/verifiable'
require 'license/management/python/pipenv'
require 'license/management/python/pypi'
require 'license/management/repository'
require 'license/management/report'
require 'license/management/version'

# This applies a monkey patch to the JsonReport found in the `license_finder` gem.
LicenseFinder::JsonReport.prepend(License::Management::Report)

# This monkey patch can be removed once https://github.com/pivotal/LicenseFinder/pull/659 is released
LicenseFinder::Scanner.const_set(
  :PACKAGE_MANAGERS,
  LicenseFinder::Scanner::PACKAGE_MANAGERS + [License::Management::Pipenv]
)

# This monkey patch can be removed once we upgrade to license_finder 5.9.2. Details [here](https://gitlab.com/gitlab-org/gitlab/issues/13748#note_235810786).
module LicenseFinder
  class Bundler < PackageManager
    def definition
      @definition ||=
        begin
          Dir.chdir(project_path.to_s) do
            ::Bundler::Definition.build(detected_package_path, lockfile_path, nil)
          end
        end
    end
  end
end

module License
  module Management
    def self.root
      Pathname.new(File.dirname(__FILE__)).join('../..')
    end

    def self.http
      @http ||= Net::Hippie::Client.new.tap do |client|
        client.logger = ::Logger.new('http.log')
        client.follow_redirects = 3
      end
    end
  end
end
